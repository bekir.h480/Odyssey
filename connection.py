from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from models.base import Base


engine = create_engine('sqlite:///test_5.db')
Base.metadata.create_all(bind=engine)
Session = sessionmaker(bind=engine)
session = Session()


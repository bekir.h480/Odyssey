from Components.actions import take_item, drop_item, clear_screen, list_items
from Components.colors import RED, YELLOW, RESET_COLOR, BLUE
from Components.combat import initiate_combat
from Components.generators import generate_enemy, generate_items
from Entity.player import Player
from Entity.room import Room

player = Player(name="YOU")
room = Room(number=1)
room.Contents = generate_items(2)


def handle_action(input_msg: str):
    action, subject = input_msg.split()
    subject = subject.strip()
    if action == "take":
        take_item(player=player, room=room, item_name=subject)
    elif action == "drop":
        drop_item(player=player, room=room, item_name=subject)
    elif action == "list":
        list_items(player=player, room=room, subject=subject)
    else:
        print("no such action!")


clear_screen()
print(room)
room.list_contents()


while True:
    input_message = input("\n>> ")
    clear_screen()

    if input_message == "exit":
        break
    elif input_message == "fight":
        room = initiate_combat(player, generate_enemy(), room)
    else:
        handle_action(input_message)

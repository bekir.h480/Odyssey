from Components.colors import RED, YELLOW, BLUE, RESET_COLOR, GREEN
from Entity.player import Player
from Entity.room import Room
import os


def clear_screen():
    os.system("cls")


def capacity_check(player: Player, item_weight: int):
    if (player.calculate_current_capacity() + item_weight) > player.Inventory_max_capacity:
        print(YELLOW + "Item is too " +
              RED + "heavy" +
              YELLOW + "! \n" + RESET_COLOR)
        return False
    else:
        return True


def take_item(player: Player, room: Room, item_name: str):
    item = room.extract_item_from_contents(item_name)
    if item is None:
        return
    if capacity_check(player, item.Weight):
        player.add_item_to_inventory(item)
        room.remove_item_from_contents(item)
    player.list_inventory()
    print("*---------------------------*\n")
    room.list_contents()


def drop_item(player: Player, room: Room, item_name: str):
    item = player.extract_item_from_inventory(item_name)
    if item is None:
        return
    player.drop_item_from_inventory(item)
    room.add_item_to_contents(item)
    player.list_inventory()
    print("*---------------------------*\n")
    room.list_contents()


def list_items(player: Player, room: Room, subject: str):
    if subject.lower() == "all":
        player.list_inventory()
        room.list_contents()
    elif subject.lower() == "inventory":
        player.list_inventory()
    elif subject.lower() == "room":
        room.list_contents()
    else:
        print(RED + "You can only list " +
              YELLOW + "Inventory " +
              RED + "and " +
              BLUE + "Room" +
              RED + "!" + RESET_COLOR)

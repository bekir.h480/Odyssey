import math
import time
from Components.actions import clear_screen
from Components.colors import GREEN, RESET_COLOR, RED, YELLOW, BLUE
from Entity.enemy import Enemy
from Entity.player import Player
from Entity.room import Room

player = Player(name="placeholder")
enemy = Enemy(name="placeholder", size=0)
room = Room(number=0)

player_stamina_buffer = 0
enemy_stamina_buffer = 0


def calculate_damage(attack: int, defense: int):
    if defense >= 0:
        mitigation_factor = 1 - math.log(1 + defense, 10) / 4
    else:
        mitigation_factor = 1 + math.log(1 - defense, 10) / 4
    damage_after_mitigation = attack * mitigation_factor
    return round(damage_after_mitigation, 1)


def player_successful_hit():
    damage = calculate_damage(player.Attack, enemy.Defense)
    print(
        GREEN + "Hit! " +
        RED + f"{enemy.Name} " +
        RESET_COLOR + f"{enemy.Health_current}" +
        GREEN + "♥ --> " +
        RESET_COLOR + f"{round(enemy.Health_current - damage, 1)}" +
        RED + "♥ | " +
        YELLOW + f"mitigated[{round(player.Attack - damage, 1)}]" + RESET_COLOR
        )
    enemy.Health_current -= damage
    enemy.Health_current = round(enemy.Health_current, 1)


def enemy_successful_hit():
    damage = calculate_damage(enemy.Attack, player.Defense)
    print(
        RED + "Hit! " +
        GREEN + f"{player.Name} " +
        RESET_COLOR + f"{player.Health_current}" +
        GREEN + "♥ --> " +
        RESET_COLOR + f"{round(player.Health_current-enemy.Attack, 1)}" +
        RED + "♥ | " +
        YELLOW + f"mitigated[{round(enemy.Attack - damage, 1)}]" + RESET_COLOR
        )
    player.Health_current -= damage
    player.Health_current = round(player.Health_current, 1)


def reset_stamina():
    player.Stamina_current = player.Stamina_max - player_stamina_buffer
    enemy.Stamina_current = enemy.Stamina_max - enemy_stamina_buffer


def player_stamina_check():
    if player.Stamina_current == 0 and enemy.Stamina_current > 0:
        return False
    elif player.Stamina_current == 0 and enemy.Stamina_current == 0:
        reset_stamina()
        return True
    return True


def enemy_stamina_check():
    if enemy.Stamina_current == 0 and player.Stamina_current > 0:
        return False
    elif enemy.Stamina_current == 0 and player.Stamina_current == 0:
        reset_stamina()
        return True
    return True


def player_stamina_drain(amount: int):
    global player_stamina_buffer
    if (player.Stamina_current - amount) < 0:
        player_stamina_buffer = abs(player.Stamina_current - amount)
        player.Stamina_current = 0
    else:
        player.Stamina_current -= amount


def enemy_stamina_drain(amount: int):
    global enemy_stamina_buffer
    if (enemy.Stamina_current - amount) < 0:
        enemy_stamina_buffer = abs(enemy.Stamina_current - amount)
        enemy.Stamina_current = 0
    else:
        enemy.Stamina_current -= amount


def player_attacks():
    print(GREEN + f"{player.Name} " +
          YELLOW + "ATTACK: " + RESET_COLOR)
    if player_stamina_check():
        player_stamina_drain(20)
        player_successful_hit()
    else:
        print(RED + f"Exhausted! {player.Stamina_current}" +
              RESET_COLOR + "/" +
              GREEN + f"{player.Stamina_max} " +
              YELLOW + "⚡ \n" + RESET_COLOR)


def enemy_attacks():
    print(RED + f"{enemy.Name} " +
          YELLOW + "ATTACK: " + RESET_COLOR)
    if enemy_stamina_check():
        enemy_stamina_drain(20)
        enemy_successful_hit()
    else:
        print(RED + f"Exhausted! {enemy.Stamina_current}" +
              RESET_COLOR + "/" +
              GREEN + f"{enemy.Stamina_max} " +
              YELLOW + "⚡ \n" + RESET_COLOR)


def player_death():
    clear_screen()
    print(YELLOW + "!*****************!!!*****************!\n")
    print(RED + f"\t YOU DIED! \n" +
          YELLOW + f"\t KILLED BY: " +
          RED + f"{enemy.Name} \n" +
          RESET_COLOR + f"\t IN " +
          BLUE + f"ROOM {str(room.Number)}\n")
    print(YELLOW + "!*****************!!!*****************!" + RESET_COLOR)


def combat_rewards():
    clear_screen()
    print(BLUE + "!*******************!!!*******************!\n")
    print(GREEN + "\t\tYOU WIN! \n" +
          YELLOW + f"\t  You killed a " +
          RED + f"{enemy.Name} \n" +
          YELLOW + f"    The " +
          RED + f"{enemy.Name} " +
          YELLOW + f"dropped some " +
          BLUE + f"EQUIPMENT\n" + RESET_COLOR)
    print(BLUE + "!*******************!!!*******************!\n" + RESET_COLOR)
    new_room = Room(number=room.Number+1, contents=enemy.Equipment)
    new_room.list_contents()
    player.Health_current = player.Health_max
    return new_room


def print_state():
    clear_screen()
    print("------------------------------------------")
    print(player)
    print("------------------------------------------")
    print(enemy)
    print("------------------------------------------")


def initiate_combat(_player: Player, _enemy: Enemy, _room: Room):
    global player
    global enemy
    global room
    player = _player
    enemy = _enemy
    room = _room
    player.update_stats()
    enemy.update_stats()

    while True:
        print_state()
        time.sleep(2)

        player_attacks()
        time.sleep(2)

        if enemy.Health_current <= 0:
            break

        print_state()
        time.sleep(2)

        enemy_attacks()
        time.sleep(2)

        if player.Health_current <= 0:
            break

        time.sleep(2)
        clear_screen()

    if player.Health_current <= 0:
        player_death()
    else:
        return combat_rewards()

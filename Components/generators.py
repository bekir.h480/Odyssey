from Entity.enemy import Enemy
from Entity.item import Item
from Entity.stat import Stat
from models.enemy import EnemyModel
from models.item import ItemModel
from sqlalchemy import func
from connection import session
from models.stat import StatModel


def generate_items(amount: int):
    item_list = []
    for i in range(amount):
        item_model = session.query(ItemModel).order_by(func.random()).limit(1).first()
        if item_model is None:
            return
        item = Item.from_model(item_model)
        item.Stats = generate_stats(item.Name)
        item_list.append(item)
    return item_list


def generate_enemy():
    enemy = session.query(EnemyModel).order_by(func.random()).limit(1).first()
    if enemy is None:
        return
    return Enemy(name=enemy.name, size=enemy.size, equipment=generate_items(2))


def generate_stats(item_name: str):
    # stat = session.query(StatModel).filter(StatModel.item_name == item_name).order_by(func.random()).limit(1).first()
    # if stat is None:
    #     return
    # return Stat(item_name=stat.item_name, status=stat.status, value=stat.value, name=stat.name)7
    stats = session.query(StatModel).filter(StatModel.item_name == item_name)
    item_stats = []
    for stat in stats:
        item_stats.append(Stat.from_model(stat))
    return item_stats

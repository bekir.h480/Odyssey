from models.base import Base
from sqlalchemy import Column, String, Integer, Boolean


class StatModel(Base):
    __tablename__ = "stats"

    id = Column(Integer, primary_key=True)
    item_name = Column(String, nullable=False)
    status = Column(Boolean, nullable=False)
    value = Column(Integer, nullable=False)
    name = Column(String, nullable=False)

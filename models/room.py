from sqlalchemy import Column, Integer, String
from models.base import Base


class RoomModel(Base):
    __tablename__ = 'rooms'

    id = Column(Integer, primary_key=True)
    name = Column(String, nullable=False)

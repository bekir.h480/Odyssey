from models.base import Base
from sqlalchemy import Column, Integer, String


class EnemyModel(Base):
    __tablename__ = "enemies"

    id = Column(Integer, primary_key=True)
    name = Column(String, nullable=False)
    size = Column(Integer, nullable=False)

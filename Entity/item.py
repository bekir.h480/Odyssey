from typing import List
from Components.colors import GREEN, RESET_COLOR, YELLOW, RED
from Entity.stat import Stat


class Item:
    Name: str
    Weight: float
    Item_type: str
    Stats: List[Stat]

    def __init__(self, name: str, weight: float, item_type: str, stats=None):
        if stats is None:
            stats = []
        self.Name = name
        self.Weight = weight
        self.Item_type = item_type
        self.Stats = stats

    @classmethod
    def from_model(cls, item_model):
        return cls(
            name=item_model.name,
            weight=item_model.weight,
            item_type=item_model.item_type
        )

    def stats_output(self):
        buffer = ""
        for stat in self.Stats:
            buffer += stat.__str__()
            buffer += '\n'
        return buffer

    def __str__(self):
        return YELLOW + (f"{self.Item_type} " +
                         GREEN + f"{self.Name} - " +
                         RESET_COLOR + f"{self.Weight} " +
                         YELLOW + f"kg\n" +
                         RED + f"{self.stats_output()}") + RESET_COLOR

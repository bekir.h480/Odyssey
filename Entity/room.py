from typing import List

from Components.colors import BLUE, RESET_COLOR, RED
from Entity.item import Item


class Room:
    Number: int
    Contents: List[Item]

    def __init__(self, number: int, contents=None):
        if contents is None:
            contents = []
        self.Number = number
        self.Contents = contents

    def __str__(self):
        return (BLUE + f"------------------------------------------\n"
                f"Room number: {self.Number}"
                f"\n------------------------------------------\n" + RESET_COLOR)

    def list_contents(self):
        if not self.Contents:
            print(BLUE + "\nRoom is " +
                  RED + "empty" +
                  BLUE + "!\n" + RESET_COLOR)
        else:
            print(BLUE + "Room contents: \n" + RESET_COLOR)
            for item in self.Contents:
                print(item)

    def add_item_to_contents(self, item: Item):
        if item is not None:
            self.Contents.append(item)

    def remove_item_from_contents(self, item: Item):
        if item is not None:
            self.Contents.remove(item)

    def extract_item_from_contents(self, item_name: str):
        for item in self.Contents:
            if item.Name.lower() == item_name.lower():
                return item
        print(RED + "Select item is not in current room!" + RESET_COLOR)

from models.stat import StatModel


class Stat:
    Item_name: str
    Status: bool
    Value: int
    Name: str

    def __init__(self, item_name: str, status: bool, value: int, name: str):
        self.Item_type = item_name
        self.Status = status
        self.Value = value
        self.Name = name

    @classmethod
    def from_model(cls, stat_model):
        return cls(
            item_name=stat_model.item_name,
            status=stat_model.status,
            value=stat_model.value,
            name=stat_model.name
        )

    def __str__(self):
        return f"{'+' if self.Status else '-'}{self.Value} {self.Name}"

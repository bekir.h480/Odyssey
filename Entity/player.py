from typing import List

from Components.colors import YELLOW, RESET_COLOR, RED, GREEN
from Entity.item import Item


class Player:
    Name: str
    Health_max: float
    Health_current: float
    Attack: int
    Defense: int
    Stamina_max: int
    Stamina_current: int
    Inventory: List[Item]
    Inventory_max_capacity: int

    def calculate_current_capacity(self):
        capacity = 0
        if not self.Inventory:
            return capacity
        else:
            for item in self.Inventory:
                capacity += item.Weight
            return capacity

    def __init__(self, name: str, inventory=None, stamina_max: int = 50, health_max: float = 100, attack: int = 10,
                 defense: int = 0, inventory_max_capacity: int = 5):
        if inventory is None:
            inventory = []
        self.Name = name
        self.Health_max = health_max
        self.Health_current = health_max
        self.Inventory = inventory
        self.Attack = attack
        self.Defense = defense
        self.Stamina_max = stamina_max
        self.Stamina_current = stamina_max
        self.Inventory_max_capacity = inventory_max_capacity

    def __str__(self):
        return GREEN + (f"{self.Name}: " +
                        RED + f"{self.Health_current}♥ " +
                        YELLOW + f"{self.Stamina_current}/{self.Stamina_max}⚡ " +
                        RESET_COLOR + f"⚔️ {self.Attack} " +
                                      f"🛡️ {self.Defense}") + RESET_COLOR

    def update_stats(self):
        default = Player(name=self.Name)
        self.Health_max = default.Health_max
        self.Stamina_max = default.Stamina_max
        self.Attack = default.Attack
        self.Defense = default.Defense
        for item in self.Inventory:
            for stat in item.Stats:
                if stat.Name == "Attack":
                    if stat.Status:
                        self.Attack += stat.Value
                    else:
                        self.Attack -= stat.Value
                elif stat.Name == "Defense":
                    if stat.Status:
                        self.Defense += stat.Value
                    else:
                        self.Defense -= stat.Value
                elif stat.Name == "Stamina":
                    if stat.Status:
                        self.Stamina_max += stat.Value
                    else:
                        self.Stamina_max -= stat.Value
                elif stat.Name == "Max HP":
                    if stat.Status:
                        self.Health_max += stat.Value
                        self.Health_current = self.Health_max
                    else:
                        self.Health_max -= stat.Value
                        self.Health_current = self.Health_max
        self.Stamina_current = self.Stamina_max

    def list_stats(self):
        self.update_stats()
        return (f"\n{self.Health_max}" +
                RED + "♥  " +
                RESET_COLOR + f"{self.Stamina_max}" +
                YELLOW + "⚡  " +
                RESET_COLOR + f"⚔️{self.Attack}  🛡️{self.Defense}\n")

    def list_inventory(self):
        print(self.list_stats())
        if not self.Inventory:
            print(YELLOW + "\nInventory is " +
                  RED + "empty" +
                  YELLOW + "!\n" + RESET_COLOR)
        else:
            print(YELLOW + f"Inventory: " +
                  GREEN + f"{self.calculate_current_capacity()}" +
                  RESET_COLOR + f"/" +
                  RED + f"{self.Inventory_max_capacity} " +
                  YELLOW + f"kg\n"
                  + RESET_COLOR)
            for item in self.Inventory:
                print(item)

    def add_item_to_inventory(self, item: Item):
        if item is not None:
            self.Inventory.append(item)

    def drop_item_from_inventory(self, item: Item):
        if item is not None:
            self.Inventory.remove(item)

    def extract_item_from_inventory(self, item_name):
        for item in self.Inventory:
            if item.Name.lower() == item_name.lower():
                return item
        print(RED + "Select item is not in your inventory!" + RESET_COLOR)

from typing import List

from Components.colors import RED, RESET_COLOR, GREEN, YELLOW
from Entity.item import Item


class Enemy:
    Name: str
    Health_max: float
    Health_current: float
    Equipment: List[Item]
    Size: int
    Attack: int
    Defense: int
    Stamina_max: int
    Stamina_current: int

    def __init__(self, name: str, size: int, health_max: float = 100, equipment=None, attack: int = 5, defense: int = 0,
                 stamina_max: int = 50):
        if equipment is None:
            equipment = []
        self.Name = name
        self.Equipment = equipment
        self.Size = size
        self.Health_max = health_max
        self.Health_current = health_max
        self.Attack = attack
        self.Defense = defense
        self.Stamina_max = stamina_max
        self.Stamina_current = stamina_max

    def __str__(self):
        return (RED + (f"{self.Name}: " +
                       GREEN + f"{self.Health_current}♥ " +
                       YELLOW + f"{self.Stamina_current}/{self.Stamina_max}⚡ ") +
                RESET_COLOR + f"⚔️ {self.Attack} " +
                f"🛡️ {self.Defense}" + RESET_COLOR
                )

    def update_stats(self):
        self.Health_max *= self.Size/2
        self.Health_current = self.Health_max
        for item in self.Equipment:
            for stat in item.Stats:
                if stat.Name == "Attack":
                    if stat.Status:
                        self.Attack += stat.Value
                    else:
                        self.Attack -= stat.Value
                elif stat.Name == "Defense":
                    if stat.Status:
                        self.Defense += stat.Value
                    else:
                        self.Defense -= stat.Value
                elif stat.Name == "Stamina":
                    if stat.Status:
                        self.Stamina_max += stat.Value
                    else:
                        self.Stamina_max -= stat.Value
                elif stat.Name == "Max HP":
                    if stat.Status:
                        self.Health_max += stat.Value
                        self.Health_current = self.Health_max
                    else:
                        self.Health_max -= stat.Value
                        self.Health_current = self.Health_max
        self.Stamina_current = self.Stamina_max

    def list_equipment(self):
        print(RED + "Equipment: " + RESET_COLOR)
        for item in self.Equipment:
            print(item)
